using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryggsäcken
{
    class Program
    {
        static void Main(string[] args)
        {

            // Initierade variabeln ett tomt värde (utan ett tomt värde i detta läge kommer inte användaren få upp det personen skrev i menyn [1])
            string nr1 = "";
            // Initierade variabeln ett sant värde (så det blir möjligt att avsluta programmet efter att ha satt isRunning till false i case 4)
            bool isRunning = true;

            // Här innebär det att loopen börjar och villkoret isRunning är ett sant värde
            while (isRunning)
            {
                // Utskrift till konsolen, det som ligger i citationstecknen
                Console.WriteLine("[1] - Lägg till ett föremål");
                Console.WriteLine("[2] - Skriv ut innehållet ");
                Console.WriteLine("[3] - Rensa innehållet");
                Console.WriteLine("[4] - Avsluta ");
                Console.WriteLine("Välj:");

                // Initierade variabeln "input" till Console.ReadLine() vilket innebär att det användaren matar in sparas av programmet
                string input = Console.ReadLine();

                /* Här börjar switch-satsen (selektionen) med villkoret "input" så kommer det som är mellan klammerparenteserna att börja gälla, 
                respektive om du skriver 1, 2, 3 eller 4 */
                switch (input)
                {
                    // "" citationstecknen gör det till en sträng så risken att programmet kraschar om du matar in bokstäver blir noll
                    case "1":
                        // Om användaren angav siffran 1 så printas detta ut till konsolen
                        Console.WriteLine("Vänligen ange ett föremål: ");
                        // Här tilldelade jag variabeln nr1 ett värde så det som användaren skriver in här sparas
                        nr1 = Console.ReadLine();
                        // avbryter switch satsen
                        break;

                    case "2":
                        // Utskrift som kommer upp när du anger siffran 2 + det som sparades i "menyn [1]" (när du skrev siffran 1)
                        Console.WriteLine("Du skrev följande: " + nr1);
                        break;

                    case "3":               
                        /* Tilldelade variabeln ett tomt värde vilket innebär att när du skriver siffran 3 i konsolen så kan man säga att värdet rensas (blir ett tomt värde) och
                        användaren får skriva in på nytt igen */
                        nr1 = "";
                        break;

                    case "4":               
                        // Avslutas med en text
                        Console.WriteLine("Ha en bra dag! (tryck valfri knapp för att avsluta.)");
                        // "Ha en bra dag!" hinner komma upp innan programmet stängs ned direkt (tryck därefter på en valfri knapp för att avsluta)
                        Console.ReadKey();
                        // Bool variabeln är satt till false, vilket avslutar loopen
                        isRunning = false;
                        break;

                        // Övriga alternativ som inte är tillgängliga
                        default:
                
                        Console.WriteLine("Oj! Här gick något snett, tänk på att skriva in en siffra mellan 1 - 4!");
                        break;
                }

            }

        }
    }
}
